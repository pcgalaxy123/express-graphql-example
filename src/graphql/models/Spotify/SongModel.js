const {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
  GraphQLObjectType,
} = require("graphql");
const Artists = require("../../../mock/Artists");
const ArtistModel = require("./ArtistModel");

const SongModel = new GraphQLObjectType({
  name: "Song",
  description: "Sogn entity",
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    duration: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    authorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    artist: {
      type: ArtistModel,
      resolve: (song) => Artists.find((a) => song.authorId === a.id),
    },
  }),
});

module.exports = SongModel;
