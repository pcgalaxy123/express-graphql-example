/**
 * @type {{
 *          id: number,
 *          name: string,
 *          duration: number,
 *          authorId: number,
 *        }[]}
 */
const Songs = [
  {
    id: 1,
    name: "Your last day",
    duration: 254,
    authorId: 2,
  },
  {
    id: 2,
    name: "Megalovania",
    duration: 156,
    authorId: 1,
  },
];

module.exports = Songs;
