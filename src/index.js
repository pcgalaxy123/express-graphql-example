const express = require("express");
const { createHandler } = require("graphql-http/lib/use/express");
const http = require("http");
const SpotifySchema = require("./graphql/models/Spotify");

const PORT = process.env.PORT || "5000";

const app = express();
app.use(
  createHandler({
    schema: SpotifySchema,
  })
);

const server = http.createServer(app)

server.listen(+PORT);
console.log(`Graphql server started on http://localhost:${PORT}`)
