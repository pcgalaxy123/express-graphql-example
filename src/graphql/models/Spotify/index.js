const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
} = require("graphql");
const SongModel = require("./SongModel");
const ArtistModel = require("./ArtistModel");
const Artists = require("../../../mock/Artists");
const Songs = require("../../../mock/Songs");

const SpotifyModel = new GraphQLObjectType({
  name: "Spotify",
  fields: () => ({
    artists: {
      type: GraphQLList(ArtistModel),
      resolve: () => Artists,
    },
    songs: {
      type: GraphQLList(SongModel),
      resolve: () => Songs,
    },
    filterSongs: {
      type: GraphQLList(SongModel),
      args: {
        duration: {
          type: GraphQLInt,
        },
        authorId: {
          type: GraphQLInt,
        },
      },
      resolve: (_, { duration, authorId }) =>
        Songs.filter((s) => {
          if (duration) {
            if (s.duration > duration) return false;
          }
          if (authorId) {
            if (s.authorId !== authorId) return false;
          }

          return true;
        }),
    },
  }),
});

const SpotifySchema = new GraphQLSchema({
  query: SpotifyModel,
});

module.exports = SpotifySchema;
