/**
 * @type {{
 *          id: number,
 *          firstName: string,
 *          lastName: string,
 *        }[]}
 */
const Artists = [
  {
    id: 1,
    firstName: "Toby",
    lastName: "Fox",
  },
  {
    id: 2,
    firstName: "Rick",
    lastName: "Roll",
  },
];

module.exports = Artists;
