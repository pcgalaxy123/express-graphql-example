const { GraphQLNonNull, GraphQLInt, GraphQLString, GraphQLObjectType } = require("graphql");

const ArtistModel = new GraphQLObjectType({
  name: "Artist",
  description: "Artist entity",
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString),
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

module.exports = ArtistModel;
